//
//  ProductCollectionViewCell.swift
//  PriceMinisterTest
//
//  Created by Douik Mohamed Houcem on 14/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productHeaderLabel: UILabel!
    @IBOutlet weak var productBrandLabel: UILabel!
    @IBOutlet weak var productPriceNewLabel: UILabel!
    @IBOutlet weak var productPriceUsedLabel: UILabel!
    @IBOutlet weak var productPriceNewTextLabel: UILabel!
    @IBOutlet weak var productPriceUsedTextLabel: UILabel!
}
