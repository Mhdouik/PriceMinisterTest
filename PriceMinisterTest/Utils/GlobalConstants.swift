//
//  GlobalConstants.swift
//  PriceMinisterTest
//
//  Created by Douik Mohamed Houcem on 14/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

struct GlobalConstants {
    // Main StoryBoard
    static let mainStoryBoard = "Main"
    // Static Data
    static let mainUrl = "https://api.myjson.com/bins/k07iv"
    static let detailUrl = "https://api.myjson.com/bins/q0oqf"
    
    // Segues
    static let productDetailSegue = "productDetailSegue"
}
