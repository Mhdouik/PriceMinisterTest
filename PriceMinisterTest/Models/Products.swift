//
//  Products.swift
//  PriceMinisterTest
//
//  Created by Douik Mohamed Houcem on 14/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import SwiftyJSON
import RealmSwift
import Alamofire

class Products: Object {
    
    
    convenience init(data: AnyObject?) {
        self.init()
        let json = JSON(data!)
        let realm = try! Realm()
        // Delete all objects from the realm
        try! realm.write {
            realm.deleteAll()
        }
        if json != nil {
            if let result: Dictionary<String, JSON> = json["result"].dictionaryValue {
                if let products: Array<JSON> = result["products"]?.arrayValue {
                    for i in 0 ..< (products.count) {
                        let product = Product()
                        if let id = products[i]["id"].int {
                            product.id = id
                        }
                        if let headline = products[i]["headline"].string {
                            product.headline = headline
                        }
                        if let caption = products[i]["caption"].string {
                            product.caption = caption
                        }
                        if let bestPrice = products[i]["newBestPrice"].double {
                            product.bestPrice = String(bestPrice)
                        }
                        if let usedBestPrice = products[i]["usedBestPrice"].double {
                            product.usedBestPrice = String(usedBestPrice)
                        }
                        //image URL
                        if let imageProduct = try? products[i]["imagesUrls"][0].string {
                            product.imageUrl = imageProduct!
                        }
                        if let desc = products[i]["description"].string {
                            product.desc = desc
                        }
                        if let reviewsAverageNote = products[i]["reviewsAverageNote"].string {
                            product.reviewsAverageNote = reviewsAverageNote
                        }
                        if let nbReviews = products[i]["nbReviews"].string {
                            product.nbReviews = nbReviews
                        }
                        // Save Product
                        try! realm.write {
                            realm.add(product, update: false)
                        }
                    }
                }
                
            }
        }
    }
    
}

