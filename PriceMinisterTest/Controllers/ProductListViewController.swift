//
//  ProductsListViewController.swift
//  PriceMinisterTest
//
//  Created by Douik Mohamed Houcem on 14/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import AlamofireImage

private let reuseIdentifier = "productCollectionViewCell"
private let reusableViewIdentifier = "productListCollectionReusableView"
private let segueDetailProduct = "productDetailSegue"

class ProductListViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var filteredProductList: Results<Product>! = nil
    
    // var filteredProductList: Results<Product>!
    var filteredProductListCount = 0
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Produits"
        loadProducts()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Call Products Api
    func loadProducts() {
        let restApiProduct: RestApiProduct = RestApiProduct()
        restApiProduct.searchProductsByName(callback: {(products) -> () in
            self.filteredProductList = self.realm.objects(Product.self)
            self.filteredProductListCount = self.filteredProductList.count
            self.collectionView.reloadData()
        })
    }
    
    // Search Products from DataBase
    func searchProducts(word: String) {
        if word.isEmpty {
            self.filteredProductList = self.realm.objects(Product.self)
        } else {
            self.filteredProductList = self.realm.objects(Product.self).filter("headline contains %@", word)
        }
        self.filteredProductListCount = self.filteredProductList.count
        self.collectionView.reloadData()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ProductListViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filteredProductListCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ProductCollectionViewCell
        let imageUrl = self.filteredProductList[indexPath.row].imageUrl
        if imageUrl != "" {
            Alamofire.request(imageUrl).responseImage { response in
                if let image = response.result.value {
                    cell.productImageView.image = image
                }
            }
        }
        cell.productHeaderLabel.text = self.filteredProductList[indexPath.row].headline
        cell.productBrandLabel.text = self.filteredProductList[indexPath.row].caption
        cell.productPriceNewLabel.text = self.filteredProductList[indexPath.row].bestPrice
        cell.productPriceUsedLabel.text = self.filteredProductList[indexPath.row].usedBestPrice
        
        if self.filteredProductList[indexPath.row].usedBestPrice.elementsEqual("0.0") {
            cell.productPriceUsedLabel.isHidden = true
            cell.productPriceUsedTextLabel.isHidden = true
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionElementKindSectionHeader) {
            let headerView:UICollectionReusableView =  collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: reusableViewIdentifier, for: indexPath)
            
            return headerView
        }
        
        return UICollectionReusableView()
    }
    
    
    // MARK: UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: segueDetailProduct, sender: nil)
    }
    
}

extension ProductListViewController: UISearchBarDelegate {
    
    // MARK: UISearchBarDelegate
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if(!(searchBar.text?.isEmpty)!){
            //reload your data source if necessary
            self.collectionView?.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchProducts(word: searchText)
    }
}
