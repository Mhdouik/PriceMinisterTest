//
//  Product.swift
//  PriceMinisterTest
//
//  Created by Douik Mohamed Houcem on 14/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import RealmSwift

class Product: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var headline = "" // Product Title
    @objc dynamic var caption = "" // Product Brand
    @objc dynamic var bestPrice = "" // Best price for product
    @objc dynamic var usedBestPrice = "" // Best price for old product
    @objc dynamic var imageUrl = "" // Product images
    @objc dynamic var desc = "" // Product description
    @objc dynamic var reviewsAverageNote = "" // Product average reviews
    @objc dynamic var nbReviews = "" // Product reviews number
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
}
