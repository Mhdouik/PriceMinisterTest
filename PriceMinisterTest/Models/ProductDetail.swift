//
//  ProductDetail.swift
//  PriceMinisterTest
//
//  Created by Douik Mohamed Houcem on 08/03/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import SwiftyJSON
import RealmSwift
import Alamofire

class ProductDetail: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var headline = "" // Product Title
    @objc dynamic var productDetailTitle = "" // P roduct Detail Title
    @objc dynamic var caption = "" // Product Brand
    @objc dynamic var bestPrice = "" // Best price for product
    @objc dynamic var usedBestPrice = "" // Best price for old product
    let imageUrl = List<String>() // Product images
    @objc dynamic var desc = "" // Product description
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(data: AnyObject?) {
        self.init()
        let json = JSON(data!)
        let realm = try! Realm()
        // Delete ProductDetail from the realm
        try! realm.write {
            let resultsProductDetail: Results<ProductDetail> = realm.objects(ProductDetail.self)
            realm.delete(resultsProductDetail)
        }
        if json != nil {
            if let result: Dictionary<String, JSON> = json["result"].dictionaryValue {
                let product = ProductDetail()
                if let id = result["id"]?.int {
                    product.id = id
                }
                if let headline = result["headline"]?.string {
                    product.headline = headline
                }
                if let caption = result["caption"]?.string {
                    product.caption = caption
                }
                if let bestPrice = result["newBestPrice"]?.double {
                    product.bestPrice = String(bestPrice)
                }
                if let usedBestPrice = result["usedBestPrice"]?.double {
                    product.usedBestPrice = String(usedBestPrice)
                }
                // images URLs
                if let imagesProduct: Array<JSON> = result["imagesUrls"]?.arrayValue {
                    print(imagesProduct)
                    for i in 0..<imagesProduct.count-1 {
                        print(i)
                        let productImageUrl: String = imagesProduct[i].stringValue
                        product.imageUrl.append(productImageUrl)
                    }
                }
                if let desc = result["description"]?.string {
                    product.desc = desc
                }
                try! realm.write {
                    realm.add(product, update: false)
                }
            }
        }
    }
    
}

