//
//  RestApiProduct.swift
//  PriceMinisterTest
//
//  Created by Douik Mohamed Houcem on 14/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import Alamofire

class RestApiProduct {
    
    // Get All Products Request
    func searchProductsByName(callback: @escaping (Products?)->()) {
        let url = GlobalConstants.mainUrl
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding(destination: .methodDependent), headers: nil)
            .responseJSON { response in
                if let JSON = response.result.value {
                    callback(Products(data: JSON as AnyObject?))
                }
        }
    }
    
    // Get Product Detail Request
    func getProductDetail(callback: @escaping (ProductDetail?)->()) {
        let url = GlobalConstants.detailUrl
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding(destination: .methodDependent), headers: nil)
            .responseJSON { response in
                if let JSON = response.result.value {
                    callback(ProductDetail(data: JSON as AnyObject?))
                }
        }
    }
    
}
