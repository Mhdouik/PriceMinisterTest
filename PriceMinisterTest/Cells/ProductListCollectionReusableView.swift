//
//  ProductListCollectionReusableView.swift
//  PriceMinisterTest
//
//  Created by Douik Mohamed Houcem on 14/02/2018.
//  Copyright © 2018 Douik Mohamed Houcem. All rights reserved.
//

import UIKit

class ProductListCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
}
